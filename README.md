# Proyecto: Materias

Registro de vehículos y propietarios con Laravel, Bootstrap, Javascript, MYSQL, PHP


### Pre-requisitos 📋

```
php 7.4
mysql
composer
```

### Instalación 🔧

configurar el archivo .env

```
    Clone el repositorio
    Crear la base de datos: proyecto_materias
    Diríjase a la carpeta del proyecto con: cd ./proyecto_materias
    Copiar el archivo .env a patir de el archivo .env.example y rellenar las credenciales de la base de datos
    Ejecutar en la cónsola composer install
    Ejecutar en la cónsola php artisan key:generate
    Ejecutar en la cónsola php artisan migrate:fresh --seed
    Ejecutar en la cónsola php artisan serve
    Ingresar en el explorador localhost:8000
    Listo
```
_

### Data Inicial 🔧

```
    Administrador

    Usuario: admin@test.com
    clave: password

    Nota: todos los usuarios creados por el administrador tienen como clave password (Para prueba)

    Ejemplo

    Usuario: peter@parker.com
    clave: password

    Usuario: steve@rogers.com
    clave: password

    Usuario: nat@romanoff.com
    clave: password
    
```
_
### URL Producción

https://proyecto-materias.000webhostapp.com/


## Construido con 🛠️

* [PHP]
* [MYSQLI]
* [MYSQL]
* [APACHE2]
* [Composer]
* [Laravel]
* [Bootstrap]
* [jQuery]

## Desarrollado por:
María Beatriz Rondón de Lockwood

## Licencia 📄

Este proyecto está bajo la Licencia GNU General Public License