<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Subject;

$factory->define(Subject::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(['Matematicas','Historia','Ciencias','Geografia','Arte','Quimica','Fisica', 'Biologia', 'Deportes', 'Literatura'])
    ];
});
