<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'name' => 'Admin Admin',
        'identification' => 12345678,
        'phone' => '(1) 2346543', // password
        'user_id' => 1,
    ];
});