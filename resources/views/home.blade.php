@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <div class="row">
                    <div class="col-lg-12 col-md-12 grid-margin">
                        <div class="card">
                        <div class="card-body">
                            <div class="d-sm-flex justify-content-between align-items-center mb-4">
                                <div id="carouselExampleFade" class="carousel slide carousel-fade w-100" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                    <img src="https://www.syf.pe/wp-content/uploads/2016/10/online1.jpg" class="d-block w-100" alt="...">
                                    </div>
                                    <div class="carousel-item">
                                    <img src="https://www.welivesecurity.com/wp-content/uploads/2019/10/capacitacion.jpg" class="d-block w-100" alt="...">
                                    </div>
                                    <div class="carousel-item">
                                    <img src="https://www.altonivel.com.mx/wp-content/uploads/2017/06/Graduacion.jpg" class="d-block w-100" alt="...">
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 col-xl-6 col-xs-12 grid-margin stretch-card">
                        <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Tareas Pendientes</h4>
                            <div class="add-items d-flex">
                            <input type="text" class="form-control todo-list-input" placeholder="What do you need to do today?">
                            <button class="add btn btn-primary font-weight-medium todo-list-add-btn">Agregar</button>
                            </div>
                            <div class="list-wrapper">
                            <ul class="d-flex flex-column-reverse todo-list todo-list-custom">
                                <li class="completed">
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input class="checkbox" type="checkbox" checked> Realizar diagramas </label>
                                </div>
                                <i class="remove mdi mdi-close-circle-outline"></i>
                                </li>
                                <li>
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input class="checkbox" type="checkbox"> Registrar nuevos estudiantes </label>
                                </div>
                                <i class="remove mdi mdi-close-circle-outline"></i>
                                </li>
                                <li>
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input class="checkbox" type="checkbox"> Iniciar clase virtual </label>
                                </div>
                                <i class="remove mdi mdi-close-circle-outline"></i>
                                </li>
                                <li class="completed">
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input class="checkbox" type="checkbox" checked> Preparar presentación </label>
                                </div>
                                <i class="remove mdi mdi-close-circle-outline"></i>
                                </li>
                            </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-6 col-xs-12 grid-margin stretch-card">
                        <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Reuniones</h4>
                            <div class="add-items d-flex">
                            <input type="text" class="form-control todo-list-input" placeholder="What do you need to do today?">
                            <button class="add btn btn-primary font-weight-medium todo-list-add-btn">Agregar</button>
                            </div>
                            <div class="list-wrapper">
                            <ul class="d-flex flex-column-reverse todo-list todo-list-custom">
                                <li class="completed">
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input class="checkbox" type="checkbox" checked> Reunión de Matemáticas </label>
                                </div>
                                <i class="remove mdi mdi-close-circle-outline"></i>
                                </li>
                                <li>
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input class="checkbox" type="checkbox"> Reunión con estudiantes </label>
                                </div>
                                <i class="remove mdi mdi-close-circle-outline"></i>
                                </li>
                                <li>
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input class="checkbox" type="checkbox"> Reunión de dirección </label>
                                </div>
                                <i class="remove mdi mdi-close-circle-outline"></i>
                                </li>
                            </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
