<form action="" class="form-horizontal" novalidate>
      <div class="row">
      <div class="col-lg-12 text-center">
        <span>Materias</span>
      </div>
    </div>
    <div class="row justify-content-center pt-2">
      <div class="col-lg-12 text-center">
        <div class="item form-group">
          <select name="subjects[]" id="subjects" multiple="multiple">
            @foreach ($array['data'] as $key => $subject)
              @if($subject['selected']==1)
                <option selected value="{{ $subject['id'] }}">{{ $subject['name'] }}</option>
              @else
                <option value="{{ $subject['id'] }}">{{ $subject['name'] }}</option>
              @endif
            @endforeach
          </select>
        </div>
      </div>
    </div>

    <div class="row justify-content-center p-2">
      <div class="col-lg-12">
        <div class="form-group text-center pt-2">
            <div class="form-group">
              <a class="btn btn-primary mr-2" href="{{ route('pages.inscription.index') }}">Reestablecer</a>
              {!! Form::submit('Guardar',['class' => 'btn btn-success', 'id' => 'guardar']) !!}
            </div>
        </div>
      </div>      
</form>