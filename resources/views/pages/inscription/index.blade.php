@extends('layouts.master')

@section('content')

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
   @include('flash::message')
    <div class="card">
      <div class="card-body content-student">
        {!! Form::model($array, ['route'=> ['inscription.update', $array['dataStudent']->id],'method' => 'PUT']) !!}
           @include('pages.inscription.partials.form')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
</div>

@endsection