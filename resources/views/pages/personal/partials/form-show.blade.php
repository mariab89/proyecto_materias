<form action="" class="form-horizontal" novalidate>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12 text-center pt-5">
            <h4>Datos personales</h4>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <div class="item form-group">
              {!! Form::label('name','Nombre') !!}
              {!! Form::text('name', $students->name, ['required', 'class' => 'form-control text-capitalize', 'readonly', 'maxlength' => '500',]) !!}
            </div>
            <div class="item form-group">
              {!! Form::label('identification','Identificacion') !!}
              {!! Form::text('identification',$students->identification, ['required', 'class' => 'form-control', 'readonly', 'maxlength' => '500',]) !!}
            </div>
            <div class="item form-group">
              {!! Form::label('phone','Celular') !!}
              {!! Form::text('phone',$students->phone, ['required', 'class' => 'form-control', 'readonly', 'maxlength' => '500',]) !!}
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-12 text-center pt-5">
            <h4>Datos de la cuenta</h4>
          </div>
        </div>
        <div class="row justify-content-center pt-2">
          <div class="col-lg-6">
            <div class="item form-group">
              {!! Form::label('email','Email') !!}
              {!! Form::email('email',$students->email, ['required', 'class' => 'form-control', 'readonly', 'maxlength' => '30',]) !!}
            </div>
          </div>
        </div>
    </div>
</form>