@extends('layouts.master')

@section('content')

  @include('pages.errors')
  {!! Form::open(['route'=> 'student.store']) !!}
    @include('pages.student.partials.form')
  {!!Form::close()!!}

@endsection
