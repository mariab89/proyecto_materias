<form action="" class="form-horizontal" novalidate>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <h2>Estudiantes</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 text-center">
            <span>Datos personales</span>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <div class="item form-group">
              {!! Form::label('name','Nombre') !!}
              {!! Form::text('name',null, ['required', 'class' => 'form-control text-capitalize', 'maxlength' => '500',]) !!}
            </div>
            <div class="item form-group">
              {!! Form::label('identification','Identificacion') !!}
              {!! Form::text('identification',null, ['required', 'class' => 'form-control', 'maxlength' => '500',]) !!}
            </div>
            <div class="item form-group">
              {!! Form::label('phone','Celular') !!}
              {!! Form::text('phone',null, ['required', 'class' => 'form-control', 'maxlength' => '500',]) !!}
            </div>
          </div>
        </div>
         <div class="row">
          <div class="col-lg-12 text-center">
            <span>Materias</span>
          </div>
        </div>
        <div class="row justify-content-center pt-2">
          <div class="col-lg-4 text-center">
            <div class="item form-group">
              <select name="subjects[]" id="subjects" multiple="multiple" required>
                @foreach ($subject as $key => $subject)
                  <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 text-center p-2">
            <span>Datos de la cuenta</span>
          </div>
        </div>
        <div class="row justify-content-center p-2">
          <div class="col-lg-6">
            <p>La clave será enviada al correo ingresado</p>
            <div class="item form-group">
              {!! Form::label('email','Email') !!}
              {!! Form::email('email',null, ['required', 'class' => 'form-control', 'maxlength' => '30',]) !!}
            </div>
            <div class="form-group float-right pt-2">
                <div class="form-group">
                  <a class="btn btn-primary mr-2" href="{{ route('pages.student.index') }}">Retornar</a>
                  {!! Form::submit('Guardar',['class' => 'btn btn-success', 'id' => 'guardar']) !!}
                </div>
            </div>
          </div>
        </div>
    </div>
</form>