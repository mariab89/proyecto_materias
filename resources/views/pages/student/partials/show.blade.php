@section('content')
<div class="row">        
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        @include('flash::message')
        <div class="row">
          <div class="col-md-4">
            <p>Marca: {{ $brand->name }}</p>
          </div>
          <div class="col-md-4">
            <p>Cantidad de Vehículos: {{ count($vehicles) }}</p>
          </div>
        </div>
        <div class="table-responsive pt-4">
          <table class="table table-striped generalDatatable">
            <thead>
              <tr>
                <th> Num </th>
                <th> Placa </th>
                <th> Propietario </th>
                <th> Tipo de Vehículo </th>
                <th> Creado </th>
                <th> Actualizado </th>
              </tr>
            </thead>
            <tbody>
               @foreach ($vehicles as $key => $vehicles)
                  <tr>
                      <td>{{ $key+1 }}</td>
                      <td class="text-uppercase">{{ $vehicles->license }}</td>
                      <td class="text-capitalize">{{ $vehicles->name_owner }}</td>
                      <td class="text-capitalize">{{ $vehicles->name_vehicle_type }}</td>
                      <td>{{ $vehicles->date_create }}</td>
                      <td>{{ $vehicles->date_update }}</td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="row pt-4">
          <div class="col-md-12">
            <a class="btn btn-info float-right" href="{{ route('pages.brand.index') }}">Retornar</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection