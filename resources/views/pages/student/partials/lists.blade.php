@section('content')
<div class="row">        
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      <div class="row">
        <div class="col-lg-12">
          <h2>Estudiantes</h2>
        </div>
      </div>
        @include('flash::message')
         <div class="row">
          <div class="col-md-12">
            <a href="{{ url('/student/create') }}" class="btn btn-primary float-right">Registrar</a>
          </div>
        </div>
        <div class="table-responsive pt-4">
          <table class="table table-striped generalDatatable">
            <thead>
              <tr>
                <th> Num </th>
                <th> Nombre </th>
                <th> Cédula </th>
                <th> Celular </th>
                <th> Correo </th>
                <th> Acciones </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($students as $key => $students)
                  <tr>
                      <td>{{ $key+1 }}</td>
                      <td class="text-capitalize">{{ $students->name }}</td>
                      <td>{{ $students->identification }}</td>
                      <td>{{ $students->phone }}</td>
                      <td>{{ $students->email }}</td>
                      <td class="col-sm-1">
                          <form action="{{ route ('student.destroy',[$students->id])}}"  method="POST">
                              {{ csrf_field() }}
                              <button class="btn btn-info btn-xs" type="button" onclick="location.href='{{ route ('show.student',[$students->id])}}'"><i class="mdi mdi-eye "></i></button>
                              <button class="btn btn-warning btn-xs" type="button" onclick="location.href='{{ route ('edit.student',[$students->id])}}'"><i class="mdi mdi-border-color "></i></button>
                              <input type="hidden" name="_method" value="DELETE">
                              <button class="btn btn-danger btn-xs" onclick="return confirm('¿Desea eliminar este registro?')"><i class=" mdi mdi-close"></i></button>
                          </form>
                      </td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection