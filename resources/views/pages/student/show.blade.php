@extends('layouts.master')

@section('content')

  {!! Form::open(['route'=> 'student.store']) !!}
    @include('pages.student.partials.form-show')
  {!!Form::close()!!}

@endsection