@extends('layouts.master')

@section('content')

  {!! Form::model($data, ['route'=> ['student.update', $data['student']->id],'method' => 'PUT']) !!}
    @include('pages.student.partials.form-edit')
  {!!Form::close()!!}

@endsection