<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- plugin css -->
      {!! Html::style('assets/plugins/@mdi/font/css/materialdesignicons.min.css') !!}
      {!! Html::style('assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
      {!! Html::style('/assets/plugins/multiselect/css/multi-select.css') !!}
      <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" />
    <!-- end plugin css -->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="master">
        <div class="container-scroller" id="master">
          @include('layouts.header')
          <div class="container-fluid page-body-wrapper">
            @include('layouts.sidebar')
            <div class="main-panel">
              <div class="content-wrapper">
                @yield('content')
              </div>
              @include('layouts.footer')
            </div>
          </div>
        </div>
    </div>
   <!-- Scripts -->
     {!! Html::script('js/app.js') !!}
    <!-- plugin js -->
  @stack('plugin-scripts')

  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  {!! Html::script('/assets/plugins/multiselect/js/jquery.multi-select.js') !!}
  {!! Html::script('assets/js/functions.js') !!}

  @stack('custom-scripts')
  
  @section('scripts') 
  @endsection
</body>
</html>
