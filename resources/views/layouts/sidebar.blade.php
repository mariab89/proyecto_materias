<nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled pt-5" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      @if(auth()->id()==1)
        <a class="nav-link" href="{{ url('/index') }}">
          <i class="menu-icon mdi mdi-account-multiple "></i>
          <span class="menu-title">Estudiantes</span>
        </a>
      @endif  
      @if(auth()->id()!=1)
      <a class="nav-link" href="{{ url('/personal/show/') }}">
        <i class="menu-icon mdi mdi-account"></i>
        <span class="menu-title">Perfil</span>
      </a>
      <a class="nav-link" href="{{ url('/inscription/index') }}">
        <i class="menu-icon mdi mdi-book-open-variant "></i>
        <span class="menu-title">Inscripción de Materias</span>
      </a>
      @endif
    </li>
  </ul>
</nav>