<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == "POST") {
            return [
                'name'=>'required',
                'email'=>'required|unique:users,email',
                'identification'=>'required|max:10|unique:students,identification',
                'phone'=>'required|max:15'
            ];
        }
        if ($this->method() == "PUT") {
            return [
                'name'=>'required',
                'phone'=>'required|max:15'
            ];
        }  

    }

    public function messages()
    {
        return [
            'name.required'=>'El campo nombre es obligatorio',
            'email.required'=>'El email es obligatorio',
            'email.unique'=>'El email ya existe',
            'identification.required'=>'El campo identificacion es obligatorio',
            'identification.unique'=>'El campo identificacion ya existe',
            'identification.max'=>'El campo identificacion no debe contener mas de 10 digitos',
        ];
    }
}
