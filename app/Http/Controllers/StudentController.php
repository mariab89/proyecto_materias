<?php

namespace App\Http\Controllers;

use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StudentRequest;
use App\Services\StudentService;
use App\Student;
use App\User;
use App\Subject;
use App\Inscription;
use Auth;

class StudentController extends Controller
{

    public function __construct(StudentService $studentService)
    {
        $this->student = new Student();
        $this->inscription = new Inscription();
        $this->studentService = $studentService;
    }

    public function index() 
    {
        $students =  $this->student->listStudent();
        return view('pages.student.index', compact('students'));
    }

    public function create()
    {
        $subject = Subject::all();
        return view('pages.student.create', compact('subject'));
    }

    public function store(StudentRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make('password'),
        ]);

        $student = Student::create([
            'name' => $request->name,
            'identification' => $request->identification,
            'phone' => $request->phone,
            'user_id' => $user->id,
        ]);
        
        $subjects = $request->subjects;

        for($i=0;$i<count($subjects);$i++)  {
            Inscription::create([
                'student_id' => $student->id,
                'subject_id' => $subjects[$i]
            ]);             
        }
       
        Flash::success('Estudiante registrado satisfactoriamente')->important();
        return redirect()->route('pages.student.index');
    }

    public function edit($id)
    {
        $data = array(
            'student' => $this->student->findStudents($id),
            'subject' => $this->studentService->getSubject($id)
        );
    
    	return view('pages.student.edit', compact('data'));
    }

    public function update(StudentRequest $request, $id)
    {
        $students = Student::find($id);
        $students->name = $request->name;
        $students->identification = $request->identification;
        $students->phone = $request->phone;
        $students->save();

        $user = User::find($students->user_id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        $inscription = Inscription::where('student_id', $id)->delete();
        
        $subjects = $request->subjects;

        for($i=0;$i<count($subjects);$i++)  {
            Inscription::create([
                'student_id' => $id,
                'subject_id' => $subjects[$i]
            ]);             
        }

        Flash::success('Registro modificado satisfactoriamente')->important();
        return redirect()->route('pages.student.index');
    }

    public function show($id)
    {
        $students = Student::find($id);
        $user = User::find($students->user_id);
        $inscription = $this->inscription->findPreInscription($id);
        return view('pages.student.show', compact('students', 'user', 'inscription'));
    }

    public function destroy($id) 
    {
        try{
            $students = Student::find($id);
            $users = User::where('id', $students->user_id)->first();
            $users->delete();
            Flash::success('Registro eliminado')->important();

        } catch(\Illuminate\Database\QueryException $e) {
            Flash::danger('No se pudo eliminar el registro')->important();
        }

        return redirect()->route('pages.student.index');
    }

    public function showPersonal()
    {
        $students = $this->student->findStudentData(Auth::id());
        return view('pages.personal.show', compact('students'));
    }

}
