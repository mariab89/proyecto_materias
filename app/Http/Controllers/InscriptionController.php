<?php

namespace App\Http\Controllers;

use Laracasts\Flash\Flash;
use App\Inscription;
use App\Student;
use App\Subject;
use Illuminate\Http\Request;
use App\Services\StudentService;
use Auth;

class InscriptionController extends Controller
{

    public function __construct(StudentService $studentService)
    {
       $this->inscription = new Inscription();
       $this->student = new Student();
       $this->studentService = $studentService;
    }

    public function index() 
    {
        $dataStudent =  $this->student->findStudentData(Auth::id());
        $data = $this->studentService->getSubjectAsigned($dataStudent->id);

        $array = array(
            'dataStudent' => $dataStudent,
            'data' => $data
        );

        return view('pages.inscription.index', compact('array'));
    }

    public function update(Request $request, $id)
    {

        $subjects = $request->subjects;

        $inscription = $this->inscription->findPreInscription($id);;

        foreach($inscription as $data) {
            $this->inscription->updateInscription($data->subject_id, $id, 0);  
        }

        if($subjects!=null) {
            for($i=0;$i<count($subjects);$i++)  {
                $this->inscription->updateInscription($subjects[$i], $id, 1);         
            }
        }

        Flash::success('Inscripción modificada satisfactoriamente')->important();
        return redirect()->route('pages.inscription.index');
    }

}
