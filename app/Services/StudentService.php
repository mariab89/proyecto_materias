<?php

namespace App\Services;

use App\Inscription;
use App\Subject;

class StudentService
{
    public function __construct()
    {
        $this->inscription = new Inscription();
    }

    public function getSubject($id)
    {
        $data = array();
        $inscription = $this->inscription->findPreInscription($id);
        $subject  = Subject::all();

        foreach($subject as $key => $sub) {
            $select = 0;
            $subj = $sub->id;
            
            foreach($inscription as $ins) {
                if($ins->subject_id === $subj) {
                    $select = 1;     
                } 
            }
            
            $data[] = array(
                'id'=> $sub->id,
                'name' => $sub->name,
                'selected' => $select
            );
        }
        
       return $data;       
    }

    public function getSubjectAsigned($id)
    {
        $data = array();
        $inscription = $this->inscription->findPreInscription($id);

        foreach($inscription as $key => $sub) {
            
            $select = 0;

            if($sub->status==1) {
                $select = 1;
            }

            $data[] = array(
                'id'=> $sub->id,
                'name' => $sub->name,
                'selected' => $select
            );
        }
       
       return $data;       
    }
}
