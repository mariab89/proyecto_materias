<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;


class Inscription extends Model
{

    protected $fillable= [

        'student_id',
        'subject_id',
        'status'
    ];

    public function findPreInscription($id)
    {
        $findPreInscription = \DB::table('inscriptions')
        ->join('students','students.id', '=','inscriptions.student_id')
        ->join('subjects','subjects.id', '=','inscriptions.subject_id')
        ->select('subjects.name', 'subjects.id', 'inscriptions.subject_id', 'inscriptions.status')
        ->where('inscriptions.student_id', '=', $id)
        ->orderBy('subjects.id', 'asc')
        ->get();

        return $findPreInscription;
    }

    public function updateInscription($subject_id, $student_id, $status)
    {
        \DB::table('inscriptions')
            ->where('inscriptions.student_id', '=', $student_id)
            ->where('inscriptions.subject_id', '=', $subject_id)
            ->update(['status' => $status]);
    }

}