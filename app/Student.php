<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;


class Student extends Model
{

    protected $fillable= [

        'name',
        'identification',
        'phone',
        'user_id'
    ];

    public function listStudent()
    {
        $listStudent = \DB::table('students')
        ->join('users','users.id', '=','students.user_id')
        ->select('students.id', 'students.name', 'students.identification', 'students.phone', 'users.email', 'users.password')
        ->where('users.id', '!=', 1)
        ->orderBy('students.id', 'desc')
        ->get();

        return $listStudent;
    }

    public function findStudents($id) 
    {
        $findStudents = \DB::table('students')
        ->join('users','users.id', '=','students.user_id')
        ->select('students.id', 'students.name', 'students.identification', 'students.phone', 'users.email', 'users.password')
        ->where('students.id', '=', $id)
        ->orderBy('students.id', 'desc')
        ->first();

        return $findStudents;
    }

    public function findStudentData($user_id) 
    {
        $findStudents = \DB::table('students')
        ->join('users','users.id', '=','students.user_id')
        ->select('students.id', 'students.name', 'students.identification', 'students.phone', 'users.email', 'users.password')
        ->where('users.id', '=', $user_id)
        ->orderBy('users.id', 'desc')
        ->first();

        return $findStudents;
    }
}
