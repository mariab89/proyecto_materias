<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('subject/{id}', function (Request $request) {
    $term = $request->term ?: '';
    $subject = DB::table('subjects')
        ->orderBy('name', 'desc')
        ->get(['id', 'name']);
    return response()->json($subject);
});