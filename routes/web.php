<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


Route::group(array('before' => 'auth'), function()
{
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/index', 'StudentController@index')->name('pages.student.index');
    Route::get('/student/create', 'StudentController@create')->name('student.create');
    Route::post('/student/store', 'StudentController@store')->name('student.store');
    Route::get('/student/{id}/edit/', 'StudentController@edit')->name('edit.student');
    Route::put('/student/{id}/update/', 'StudentController@update')->name('student.update');
    Route::get('/student/{id}/show/', 'StudentController@show')->name('show.student');
    Route::get('/personal/show/', 'StudentController@showPersonal')->name('show.personal');
    Route::delete('/student/{id}/destroy/', 'StudentController@destroy')->name('student.destroy');

    Route::get('/inscription/index', 'InscriptionController@index')->name('pages.inscription.index');
    Route::put('/inscription/{id}/update/', 'InscriptionController@update')->name('inscription.update');
});