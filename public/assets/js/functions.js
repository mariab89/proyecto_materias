
// Inicializacion

$(document).ready(function() {

    
    $('.generalDatatable').dataTable({
        "language": {
            "processing": "Procesando...",
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "No se encontraron registros",
            "info": "_START_ de _END_ (_TOTAL_)",
            "infoEmpty": "No existen registros disponibles",
            "infoFiltered": "(filtrando de un total de _MAX_ registros)",
            "infoPostFix": "",
            "search": "Buscar:",
            "url": "",
            "loadingRecords": "Cargando",
            "paginate": {
                "first":      "Primero",
                "previous":   "Anterior",
                "next":       "Siguiente",
                "last":       "Ultimo"
            }
        },
        "aria": {
            "sortAscending": "Ordenar columnas de manera ascendente",
            "sortDescending": "Ordenar columnas de manera descendiente"
        }
    });
    $.fn.dataTable.ext.errMode = 'none';

    $('#subjects').multiSelect();
});





